About
=====


This is a collection of helper scripts to accompany the [kirc](https://github.com/mcpcpc/kirc) IRC client:

  - `kirc-make`: a script to build kirc (with some minor tweaks)
  - `openssl.env`: a collection of env vars to be sourced for building socat / stunnel with the custom installed OpenSSL
  - `socat.sh`: a script to start socat relay
  - `stunnel.sh`: a script to start stunnel proxy
  - `sasl-token`: a script to generate an authentication token some IRC servers may require for the authorization
  - `kirc-run`: a script to run kirc
  - `vim/`: a Vim plugin for viewing kirc logs (upon opening the log file in Vim, use `zd` mapping to enter the channel name to view)

Make sure to inspect and adapt sources before using.
