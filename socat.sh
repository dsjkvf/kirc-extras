#!/bin/sh

export LD_LIBRARY_PATH=$HOME/usr/local/bundled/openssl/lib
$HOME/usr/local/bundled/socat/bin/socat tcp-listen:6667,reuseaddr,fork,bind=127.0.0.1 ssl:irc.freenode.net:6697,verify=0 &
