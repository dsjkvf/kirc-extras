" Local settings
set noswapfile

" Local mappings
function! irclog#Chan(...)
    if len(a:000) == 0
        let chan = input("Which channel, please? ")
    else
        let chan = a:000[0]
    endif
    " strip the leading '#'s if present
    " (chan can have 0, 1 or more leading '#'s)
    " chan string var is transformed to a binary list ('##linux' -> [1, 1, 0, 0, 0, 0, 0])
    let chan_l = map(split(chan, '\zs'), 'v:val =~ "#"')
    " the counter introduced
    let chan_c = 0
    for i in range(0, len(chan_l))
        if chan_l[i] == 0
            break
        " until the first non-'#' char, we strip leading '#'s (and increment the counter)
        else
            let chan = chan[1:]
            let chan_c += 1
        endif
    endfor
    " add/restore the leading '#'s
    let chan_c = (chan_c == 0) ? 1 : chan_c
    let com = 'Redir g/privmsg ' . repeat('#', chan_c) . '\<' . chan . '\>/'
    execute com
    only
    set filetype=ircchannel
    call ircchannel#Format()
endfunction

command! -bang -nargs=* CHAN call irclog#Chan(<f-args>)
nnoremap zd :CHAN #
