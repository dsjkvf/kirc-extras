" Local settings
setlocal wrap

" Local routine
function! ircchannel#Format()
    " make own messages look like others
    silent! %s/:privmsg /::me!\~me.me PRIVMSG /
    " remove the year from the date
    silent! %s/\d\d:\d\d:\d\d\zs \d\d\d\d\ze:://
    " remove hosts and PRIVMSG command
    silent! %s/!\~*.* PRIVMSG \#[^ :]* :/†/
    " collect the nicknames
    " (right now, the timestammp and the nickname are a single string/match)
    let ns = ['']
    for i in range(1, line('$'))
        let n = substitute(getline(i), '::\([^†]*\)†.*', '\1', '')
        call add(ns, n)
    endfor
    " find the longest nickname + the text shift to be put between the date and nicknames (1/9 of the terminal width)
    let m = max(map(copy(ns), 'len(v:val)')) + &columns / 9
    " replace every line with the same line with the shift inserted and nicknames right aligned
    for i in range(1, line('$'))
        let s = m - len(ns[i])
        call setline(i, substitute(getline(i), '::\([^†]*\)', repeat('†', s) . '\1', ''))
    endfor
    " remove temp chars
    silent! %s/†/ /g
    " set showbreak to the shift
    execute "let &l:showbreak = '" . repeat(' ', m + 1) . "'"
    " create a hidden window on the rght to center the actual text
    let r = &columns / 2 - &columns / 4
    execute "vnew +vertical\\ resize\\ -" . r
    wincmd p
    " hide status
    set laststatus=0
    " hide the split line
    hi! def link VertSplit NonText
    execute "normal! gg"
    redraw!
endfunction

" Local autocommands
autocmd! BufLeave <buffer> silent! set laststatus=2
