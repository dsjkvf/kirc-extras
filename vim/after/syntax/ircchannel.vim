" Define
syn region ircDate start=/^/ end=/\d\+ \d\d:\d\d:\d\d/
execute 'syn match ircNick "\s\{' . &columns / 9 . ',}[^ ]*\s"'

" Colorize
hi ircNick cterm=NONE ctermbg=NONE ctermfg=011 gui=NONE guibg=NONE guifg=yellow
hi! def link ircDate Comment
